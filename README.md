<style>
[fun] {color: #0ad;} /*Functions*/
[typ] {color: #d1f;} /*Types & Language Constructs*/
[num] {color: #fc4;} /*Numbers*/
[str] {color: #0b5;} /*Strings*/
[obj] {color: #f73;} /*Classes & Objects*/
[com] {color: #386; font-style: italic;} /*Comments*/
</style>
# WebLang
## Example Program in WebLang
<pre>
<code><span com>// Weblang Example Code</span>

<span fun>print</span>(<span str>"G'Day Mate!"</span>);

<span com>// Various Datatypes</span>

<span com>// Various Variable Declaration Styles</span>
<span typ>const</span> <span typ>string</span> <span var>myString</span> = <span str>"Hello!"</span>;
<span typ>const</span> myString: <span obj>String</span> = <span str>"Hello!"</span>;

<span typ>void</span> <span fun>number</span>(<span obj>Integer</span> num) {
    
};</code>
</pre>